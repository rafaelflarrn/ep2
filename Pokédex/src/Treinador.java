
public class Treinador extends Personagem {
	private int idade;
	private int pokemonsDoTreinador;
	
	private Treinador() {
	}
	
	public Treinador(String nome, int idade, int pokemonsDoTreinador) {
		super();
		this.idade = idade;
		this.pokemonsDoTreinador = pokemonsDoTreinador;
		this.nome = nome;
	}
	

	
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public int getPokemonsDoTreinador() {
		return pokemonsDoTreinador;
	}
	public void setPokemonsDoTreinador(int pokemonsDoTreinador) {
		this.pokemonsDoTreinador = pokemonsDoTreinador;
	}
/*	@Override
	public String toString() {
		return getNome();
	}*/
}