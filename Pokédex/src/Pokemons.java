
public class Pokemons extends Personagem {
	private String code;
	private String type1;
	private String type2;
	private String statsTotal;
	private String statsHp;
	private String statsAttack;
	private String statsDefense;
	private String statsSpAtk;
	private String statsSpDef;
	private String statsSpeed;
	private String statsGeneration;
	private String statsLegendary;
	private String experience;
	private String height;
	private String weight;
	private String abilitie1;
	private String abilitie2;
	private String abilitie3;
	private String move1;
	private String move2;
	private String move3;

	
	public Pokemons() {
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType1() {
		return type1;
	}
	public void setType1(String type1) {
		this.type1 = type1;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public String getStatsTotal() {
		return statsTotal;
	}
	public void setStatsTotal(String statsTotal) {
		this.statsTotal = statsTotal;
	}
	public String getStatsHp() {
		return statsHp;
	}
	public void setStatsHp(String statsHp) {
		this.statsHp = statsHp;
	}
	public String getStatsAttack() {
		return statsAttack;
	}
	public void setStatsAttack(String statsAttack) {
		this.statsAttack = statsAttack;
	}
	public String getStatsDefense() {
		return statsDefense;
	}
	public void setStatsDefense(String statsDefense) {
		this.statsDefense = statsDefense;
	}
	public String getStatsSpAtk() {
		return statsSpAtk;
	}
	public void setStatsSpAtk(String statsSpAtk) {
		this.statsSpAtk = statsSpAtk;
	}
	public String getStatsSpDef() {
		return statsSpDef;
	}
	public void setStatsSpDef(String statsSpDef) {
		this.statsSpDef = statsSpDef;
	}
	public String getStatsSpeed() {
		return statsSpeed;
	}
	public void setStatsSpeed(String statsSpeed) {
		this.statsSpeed = statsSpeed;
	}
	public String getStatsGeneration() {
		return statsGeneration;
	}
	public void setStatsGeneration(String statsGeneration) {
		this.statsGeneration = statsGeneration;
	}
	public String isStatsLegendary() {
		return statsLegendary;
	}
	public void setStatsLegendary(String statsLegendary) {
		this.statsLegendary = statsLegendary;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getAbilitie1() {
		return abilitie1;
	}
	public void setAbilitie1(String abilitie1) {
		this.abilitie1 = abilitie1;
	}
	public String getAbilitie2() {
		return abilitie2;
	}
	public void setAbilitie2(String abilitie2) {
		this.abilitie2 = abilitie2;
	}
	public String getAbilitie3() {
		return abilitie3;
	}
	public void setAbilitie3(String abilitie3) {
		this.abilitie3 = abilitie3;
	}
	public String getMove1() {
		return move1;
	}
	public void setMove1(String move1) {
		this.move1 = move1;
	}
	public String getMove2() {
		return move2;
	}
	public void setMove2(String move2) {
		this.move2 = move2;
	}
	public String getMove3() {
		return move3;
	}
	public void setMove3(String move3) {
		this.move3 = move3;
	}
	
}
