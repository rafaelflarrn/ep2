import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Pokédex {
	
	public static void main(String[] args) throws IOException {
		pokedex("Grass", 1);
	}

	public static void pokedex(String nomeDoPokemon, int op) throws IOException{
	//public static void main(String[] args) throws IOException{

		Pokemons pokemon[] = new Pokemons[722];
        String csvFile = "/home/rafael/workspace/ep2/data/csv_files/POKEMONS_DATA_1.csv";
        String csvFile2 = "/home/rafael/workspace/ep2/data/csv_files/POKEMONS_DATA_2.csv";
        BufferedReader br = null;
        BufferedReader br2 = null;
        String line = "";
        String cvsSplitBy = ",";
        int i = 0, j = 0, aux = 0, aux2 = 0;
        
        try {
        	       	
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null){
            	int aux1 = 0;           
            	pokemon[i] = new Pokemons();
                String[] data = line.split(cvsSplitBy);
                pokemon[i].setCode(data[0]);
                pokemon[i].setNome(data[1]);
                pokemon[i].setType1(data[2]);
                pokemon[i].setType2(data[3]);
                pokemon[i].setStatsTotal(data[4]);
                pokemon[i].setStatsHp(data[5]);
                pokemon[i].setStatsAttack(data[6]);
                pokemon[i].setStatsDefense(data[7]);
                pokemon[i].setStatsSpAtk(data[8]);
                pokemon[i].setStatsSpDef(data[9]);
                pokemon[i].setStatsSpeed(data[10]);
                pokemon[i].setStatsGeneration(data[11]);
                pokemon[i].setStatsLegendary(data[12]);
                switch (op) {
                	case 0:        
		                if(nomeDoPokemon == pokemon[i].getNome().intern()) {
		                	System.out.println("Entrou");
		                	aux = Integer.parseInt(pokemon[i].getCode());
		                }else {
		                	aux2 = 1;
		                }
		                break;
                	case 1: //Printar todos os pokemons de mesma tipo
		                if((nomeDoPokemon == pokemon[i].getType1().intern()) || (nomeDoPokemon == pokemon[i].getType2().intern())) {
		                	aux = Integer.parseInt(pokemon[i].getCode());
		                	aux1 = 1;
		                }
		                break;

                }
                if(aux1 == 1)
                	System.out.println(pokemon[aux].getNome());
		                	
                ++i;
            }
            br2 = new BufferedReader(new FileReader(csvFile2));
            while ((line = br2.readLine()) != null){
               String[] data = line.split(cvsSplitBy);
               pokemon[j].setExperience(data[0]);
               pokemon[j].setHeight(data[1]);
               pokemon[j].setWeight(data[2]);
               pokemon[j].setAbilitie1(data[3]);
               pokemon[j].setAbilitie2(data[4]);
               pokemon[j].setAbilitie3(data[5]);
               pokemon[j].setMove1(data[6]);
               pokemon[j].setMove2(data[7]);
               pokemon[j].setMove3(data[8]);
            ++j;
            }
            if(aux2 == 1)
            	System.out.println("Pokemon não encontrado");

          
            //System.out.println("Pokemon [Nome= " + pokemon[1].getNome() + " , Tipo=" + pokemon[1].getTipo() + "]");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (br2 != null) {
                try {
                    br2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
       //System.out.println("Pokemon [Nome= " + pokemon[aux].getNome() + " , Idade=" + pokemon[aux].getType1() + "]");

	}
	/*public static int adicionaTreinador(String nome, int q, int op) throws IOException {
		//InputStream is = System.in;
		Scanner s = new Scanner(System.in);
		ArrayList<Treinador> treinador = new ArrayList();
		//Treinador[] treinador = new Treinador[4];		
    	//treinador[q] = new Treinador();
		treinador.add(nome);
		
		if(op != 0) {
	        	treinador[q].setNome(nome);
	        	System.out.println("\nInforme a idade do treinador:");
	        	treinador[q].setIdade(s.nextInt());
	        	//System.out.println(treinador[q].getIdade());
	        	treinador[q].setPokemonsDoTreinador(1);

	        	//q++;
		}else {
			System.out.println("Nome do treinador: " + treinador.getNome() + "Idade do treinador:" + treinador[q].getIdade());
		}
		return q;
	    //System.out.println("Pokemon [Nome= " + treinador[0].getNome() + " , Idade=" + pokemon[1].getNome() + "]");
	}*/
}
